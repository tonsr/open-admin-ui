import axios from 'axios'
import qs from 'qs'
import $config from '@/config'
import {getToken} from '@/libs/util'
import {Message} from 'iview'
import {sign} from '@/libs/sign'

const service = axios.create({
  timeout: 30000
})

service.interceptors.request.use((config) => {
    // 参数签名处理
    //config = sign(config, $config.appId, $config.appSecret, 'SHA256')
    config.method === 'get' ? config.params = {...config.params} : config.data = JSON.stringify({...config.data})
    const token = getToken()
    if (token) {
      config.headers['Authorization'] = 'Bearer ' + token
    }
    return config
  }
)
/**
 * 响应结果处理
 */
service.interceptors.response.use(
  (response) => {
    if (response.data.success === true) {
      return Promise.resolve(response.data)
    } else {
      Message.error({content: response.data.message})
      return Promise.reject(response.data)
    }
  }, error => {
    let message = ''
    if (error && error.response) {
      switch (error.response.status) {
        case 401:
          location.reload()
          return
        case 403:
          message = error.response.data.path + ',' + error.response.data.message
          break
        case 429:
          message = '访问太过频繁，请稍后再试!'
          break
        default:
          message = error.response.data.message ? error.response.data.message : '服务器错误'
          break
      }
      Message.error({content: message})
      // 请求错误处理
      return Promise.reject(error)
    } else {
      message = '连接服务器失败'
      Message.error({content: message})
      return Promise.reject(error)
    }
  }
)

export default service
